<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function signup(Request $request)
    {
        $nama = $request["namaa"];
        return view('welkom', ['nama' => $nama]);
    }
}
